﻿using System;
using System.Collections.Generic;
using System.Linq;
using BanSQL;
using BanSQL.Lib;
using Smod2;
using Smod2.API;
using Smod2.Commands;

namespace Commands
{
    internal class Warn : ICommandHandler
    {
        private Main main;

        public Warn(Main main)
        {
            this.main = main;
        }

        public string GetCommandDescription()
        {
            return "Add / Delete / Get warn from player.";
        }

        public string GetUsage()
        {
            return "WARN <DELETE | ADD | GET>";
        }

        public string[] OnCall(ICommandSender sender, string[] args)
        {
            if (ConfigManager.Manager.Config.GetBoolValue("bsql_warn_enable", true))
            {
                if (args.Length >= 1)
                {
                    BanSQL.Lib.Warn warnClass = new BanSQL.Lib.Warn(main);
                    if (args[0].ToLower() == "delete")
                    {
                        if (args.Length >= 2)
                        {

                            if (int.TryParse(args[1], out int index))
                            {
                                warnClass.DelWarn(index);
                                return new string[] { LangManager.Manager.GetTranslation("command.success").Replace("\\n", "\n") };
                            }
                            else
                            {
                                return new string[] { LangManager.Manager.GetTranslation("general.invalidvalue").Replace("[error]", "parameter 3 must be a digital number.").Replace("\\n", "\n") };
                            }

                        }
                        else
                        {
                            return new string[] { "WARN DELETE <INDEX>" };
                        }
                    }
                    else if (args[0].ToLower() == "add")
                    {
                        if (args.Length >= 3)
                        {

                            string steamidTarget = "";

                            foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                            {
                                if (p.Name.ToLower().Contains(args[1].ToLower()))
                                {
                                    steamidTarget = p.SteamId;
                                }
                            }

                            if (steamidTarget == "" && args[1].Length == 17 && args[1].StartsWith("76561198"))
                            {
                                steamidTarget = args[1];
                            }

                            if (steamidTarget == "") { return new string[] { LangManager.Manager.GetTranslation("general.playernotfound").Replace("\\n", "\n") }; }

                            string reason = "";

                            int i = 2;

                            while (i != args.Length)
                            {
                                reason = reason + args[i] + " ";
                                i++;
                            }

                            string admin = "Console";
                            if (sender is Player) { Player p = (Player)sender; admin = p.Name; }

                            warnClass.AddWarn(steamidTarget, reason, admin);

                            return new string[] { LangManager.Manager.GetTranslation("command.success").Replace("\\n", "\n") };

                        }
                        else
                        {
                            return new string[] { "WARN ADD <STEAMID | PLAYER> <REASON>" };
                        }


                    }
                    else if (args[0].ToLower() == "get")
                    {
                        if (args.Length >= 2)
                        {
                            string steamidTarget = "";

                            foreach (Player p in PluginManager.Manager.Server.GetPlayers())
                            {
                                if (p.Name.ToLower().Contains(args[1].ToLower()))
                                {
                                    steamidTarget = p.SteamId;
                                }
                            }

                            if (steamidTarget == "" && args[1].Length == 17 && args[1].StartsWith("76561198"))
                            {
                                steamidTarget = args[1];
                            }

                            if (steamidTarget == "") { return new string[] { LangManager.Manager.GetTranslation("general.playernotfound") }; }

                            List<WarnObject> listWarn = warnClass.GetWarnsFromSteamID(steamidTarget);

                            string msg = (listWarn != new List<WarnObject>()) ? LangManager.Manager.GetTranslation("command.getWarn").Replace("\\n", "\n") : LangManager.Manager.GetTranslation("command.getWarn.nowarn").Replace("\\n", "\n");

                            if (listWarn.Any())
                            {
                                string listWarnString = "ID | REASON | ADMIN | DATE | EXPIRE DATE | ACTIVE\n-------------------------------------------------";
                                foreach (WarnObject warns in listWarn)
                                {
                                    listWarnString = $"{listWarnString}\n{warns.index} | {warns.warn} | {warns.admin} | {warns.dateTime.ToString("yyyy/MM/dd HH:mm:ss")} | {warns.expireTime.ToString("yyyy/MM/dd HH:mm:ss")} | {(warns.active ? LangManager.Manager.GetTranslation("general.yes") : LangManager.Manager.GetTranslation("general.no"))}\n-----------------------------------";
                                }

                                string playerName = steamidTarget;
                                try
                                {
                                    playerName = BanSQL.Lib.Misc.GetNameBySteamID(steamidTarget);
                                }
                                catch (Exception)
                                {

                                }

                                msg = msg.Replace("\\n", "\n");
                                msg = msg.Replace("[player]", playerName);
                                msg = msg.Replace("[number]", listWarn.Count.ToString());
                                msg = msg.Replace("[nbactive]", listWarn.Where(active => active.active == true).Count().ToString());
                                msg = msg.Replace("[warns]", listWarnString);
                                return new string[] { msg.TrimEnd('-') };
                            }
                            else
                            {
                                return new string[] { LangManager.Manager.GetTranslation("command.getWarn.noWarn").Replace("\\n", "\n") };
                            }

                        }
                        else
                        {
                            return new string[] { "WARN GET <STEAMID | PLAYER>" };
                        }
                    }
                    else
                    {
                        return new string[] { GetUsage() };
                    }
                }
                else
                {
                    return new string[] { GetUsage() };
                }
            }
            else
            {
                return new string[] { LangManager.Manager.GetTranslation("module.warn.disabled") };
            }
        }
    }
}