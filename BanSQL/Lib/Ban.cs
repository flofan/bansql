﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Smod2;

namespace BanSQL.Lib
{
    class Ban
    {
        public MySqlConnection connection;
        public string ipaddress = ConfigManager.Manager.Config.GetStringValue("bsql_sql_ipaddress", "127.0.0.1").TrimEnd(' ').TrimStart(' ');
        public string username = ConfigManager.Manager.Config.GetStringValue("bsql_sql_username", "root").TrimEnd(' ').TrimStart(' ');
        public string password = ConfigManager.Manager.Config.GetStringValue("bsql_sql_password", string.Empty).TrimEnd(' ').TrimStart(' ');
        public string database = ConfigManager.Manager.Config.GetStringValue("bsql_sql_database", "scpsl").TrimEnd(' ').TrimStart(' ');

        public string bantable = ConfigManager.Manager.Config.GetStringValue("bsql_sql_bantable", "ban").TrimEnd(' ').TrimStart(' ');

        Plugin plugin;

        public Ban(Plugin plugin)
        {
            this.plugin = plugin;
            this.InitConnection();
        }

        private void InitConnection()
        {
            string connectionString = $"SERVER={ipaddress}; DATABASE={database}; UID={username}; PASSWORD={password}";
            this.connection = new MySqlConnection(connectionString);
        }

        public bool CheckBDDisOn()
        {
            try
            {
                connection.Open();
                connection.Close();
                return true;
            }
            catch(MySqlException)
            {
                return false;
            }
        }

        public void CreateTable()
        {
            try
            {
                connection.Open();

                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"CREATE TABLE IF NOT EXISTS {bantable} (`id` INT(11) NOT NULL AUTO_INCREMENT,`steamid` VARCHAR(17) NOT NULL,`ip` VARCHAR(15) NOT NULL,`reason` LONGTEXT NOT NULL,`admin` VARCHAR(32) NOT NULL,`unbanTime` DATETIME NOT NULL,`dateTime` DATETIME NOT NULL, `active` BOOLEAN NOT NULL DEFAULT TRUE, PRIMARY KEY(`id`))ENGINE = InnoDB;";
                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"Error when creating table for ban.\n{e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public void AddBan(string steamid, string reason, string admin, string minutes, string ip = "no")
        {
            try
            {
                if (reason.Trim(' ') == "") { return; }

                admin = admin.Trim(' ') == "" ? "Unknown admin" : admin;

                if(steamid.Length != 17) { return; };

                connection.Open();

                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"UPDATE {bantable} SET active = FALSE WHERE steamid = '{steamid}'; INSERT INTO {bantable} (steamid, ip, reason, admin, unbanTime, dateTime) VALUES (@steamid, @ip, @reason, @admin, @unbanTime, @dateTime)";
                command.Parameters.AddWithValue("@steamid", steamid);
                command.Parameters.AddWithValue("@ip", ip);
                command.Parameters.AddWithValue("@reason", reason);
                command.Parameters.AddWithValue("@admin", admin);
                command.Parameters.AddWithValue("@datetime", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                command.Parameters.AddWithValue("@unbanTime", DateTime.Now.AddMinutes(int.Parse(minutes)).ToString("yyyy-MM-dd HH:mm:ss"));

                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"The ban {reason} for {steamid} by {admin} during {minutes} minutes doesn't work.\n {e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public void DelBan(int index)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();

                command.CommandText = $"DELETE FROM `{bantable}` WHERE  `id`= {index};";

                command.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"Cannot delete ban's index {index}.\n {e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public List<BanObject> GetBansFromSteamID(string steamid)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT * FROM {bantable} WHERE steamid = '{steamid}';";
                var dr = command.ExecuteReader();
                List<BanObject> banlist = new List<BanObject>();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        int index = dr.GetInt32(0);
                        string ip = dr.GetString(2);
                        string reason = dr.GetString(3);
                        string admin = dr.GetString(4);
                        DateTime unbanTime = dr.GetDateTime(5);
                        DateTime dateTime = dr.GetDateTime(6);
                        bool active = dr.GetBoolean(7);

                        banlist.Add(new BanObject(index, steamid, reason, admin, dateTime, unbanTime, active, ip));
                    }
                }
                connection.Close();
                return banlist;
            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"Cannot get ban for the steamID : {steamid}.\n {e}");
                plugin.Error(msg);
                connection.Close();
                return new List<BanObject>();
            }
            finally
            {
                connection.Close();
            }
        }

        public List<BanObject> GetBansFromIP(string ip)
        {
            try
            {

                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT * FROM {bantable} WHERE ip = '{ip}';";
                var dr = command.ExecuteReader();
                List<BanObject> banlist = new List<BanObject>();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        int index = dr.GetInt32(0);
                        string steamid = dr.GetString(1);
                        string reason = dr.GetString(3);
                        string admin = dr.GetString(4);
                        DateTime unbanTime = dr.GetDateTime(5);
                        DateTime dateTime = dr.GetDateTime(6);
                        bool active = dr.GetBoolean(7);

                        banlist.Add(new BanObject(index, steamid, reason, admin, dateTime, unbanTime, active , ip));
                    }
                }
                connection.Close();
                return banlist;
            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"Cannot get ban for the IP : {ip}.\n {e}");
                plugin.Error(msg);
                connection.Close();
                return new List<BanObject>();
            }
            finally
            {
                connection.Close();
            }
        }

        public BanObject SteamIDIsBanned(string steamid)
        {
            List<BanObject> banactive = GetBansFromSteamID(steamid).Where(ban => ban.active == true).ToList();
            if(banactive.Count > 0)
            {
                if(banactive[0].unbanTime < DateTime.Now)
                {
                    try
                    {
                        connection.Open();
                        MySqlCommand command = connection.CreateCommand();
                        command.CommandText = $"UPDATE {bantable} SET active = FALSE WHERE steamid = '{steamid}'";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                        msg = msg.Replace("[exception]", $"Error when checking if steamid ({steamid}) is banned.\n {e}");
                        plugin.Error(msg);
                    }
                    finally
                    {
                        connection.Close();

                    }
                    return null;
                }
                return banactive[0];
            }
            else
            {
                return null;
            }
        }

        public BanObject IPIsBanned(string ip)
        {
            List<BanObject> banactive = GetBansFromIP(ip).Where(ban => ban.active == true).ToList();
            if (banactive.Count > 0)
            {
                if (banactive[0].unbanTime < DateTime.Now)
                {
                    try
                    {
                        connection.Open();
                        MySqlCommand command = connection.CreateCommand();
                        command.CommandText = $"UPDATE {bantable} SET active = FALSE WHERE ip = '{ip}';";
                        command.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                        msg = msg.Replace("[exception]", $"Error when checking if IP ({ip}) is banned.\n {e}");
                        plugin.Error(msg);
                    }
                    finally
                    {
                        connection.Close();
                    }

                    return null;
                }

                return banactive[0];
            }
            else
            {
                return null;
            }
        }

        public void UnBanPlayerBySteamID(string steamid)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"UPDATE {bantable} SET active = FALSE WHERE steamid = '{steamid}';";
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"Error when unban {steamid}.\n{e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }

        public void UnBanPlayerByIP(string ip)
        {
            try
            {
                connection.Open();
                MySqlCommand command = connection.CreateCommand();
                command.CommandText = $"UPDATE {bantable} SET active = FALSE WHERE ip = '{ip}';";
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                string msg = plugin.GetTranslation("general.exception").Replace("\\n", "\n");
                msg = msg.Replace("[exception]", $"Error when unban {ip}.\n{e}");
                plugin.Error(msg);
            }
            finally
            {
                connection.Close();
            }
        }
    }

    public class BanObject
    {

        public int index { get; set; }
        public string ip { get; set; }
        public string steamid { get; set; }
        public string reason { get; set; }
        public string admin { get; set; }
        public DateTime dateTime;
        public DateTime unbanTime;
        public bool active;

        public BanObject(int index, string steamid, string reason, string admin, DateTime dateTime, DateTime unbanTime, bool active, string ip = "no")
        {
            this.index = index;
            this.steamid = steamid;
            this.reason = reason;
            this.admin = admin;
            this.dateTime = dateTime;
            this.unbanTime = unbanTime;
            this.ip = ip;
            this.active = active;
        }
    }
}
