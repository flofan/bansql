﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Smod2;
using Smod2.API;

namespace BanSQL.Lib
{
    class Misc
    {
        public static string GetNameBySteamID(string steamid)
        {

            string name = "";

            foreach(Player p in PluginManager.Manager.Server.GetPlayers())
            {
                if(steamid == p.SteamId)
                {
                    name = p.Name;
                }
            }

            if(name == "")
            {
                throw new System.ArgumentException("Player name cannot be found.");
            }

            return name;
        }

        public static bool IsNumeric(string txt)
        {

            return long.TryParse(txt, out var check);
        }
    }
}
